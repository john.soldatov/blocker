import {BLOCKER_WHITELIST, BLOCKER_CONSOLE_WARN} from './defines'

const notInWhitelist = function (src) {
    return (
        (!BLOCKER_WHITELIST || BLOCKER_WHITELIST.every(pattern => !pattern.test(src)))
    )
}

export const observer = new MutationObserver(function (mutations) {
    {
        for (let i = 0; i < mutations.length; i++) {
            const {addedNodes} = mutations[i];

            for (let i = 0; i < addedNodes.length; i++) {
                const node = addedNodes[i]

                // For each added script tag
                if (node.nodeType === 1 && node.tagName === 'SCRIPT' && node.src !== '') {
                    const src = node.src

                    // If the src is inside the blacklist and is not inside the whitelist
                    if (notInWhitelist(src)) {

                        // Remove the node from the DOM
                        node.parentElement && node.parentElement.removeChild(node)

                        // Console log
                        if (BLOCKER_CONSOLE_WARN) {
                            console.warn('Blocked: ' + src)
                        }
                    }
                }
            }
        }
    }
});

// Starts the monitoring
observer.observe(document, {
    subtree: true,
    attributes: true,
    childList: true,
    characterData: true
});
